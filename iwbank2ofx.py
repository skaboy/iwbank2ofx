import xlrd
import csv
import sys
import itertools as it
from operator import itemgetter

from meza.io import read_csv, IterStringIO
from csv2ofx import utils
from csv2ofx.ofx import OFX
from csv2ofx.qif import QIF

mapping = {
    'has_header': True,
    'is_split': False,
    'bank': 'IwBank',
    'currency': 'Euro',
    'delimiter': ',',
    'account': itemgetter('Account'),
    'account_id': itemgetter('Account'),
    'date': itemgetter('Date'),
    'amount': itemgetter('Amount'),
    'desc': itemgetter('Description'),
    'date_fmt': '%d/%m/%Y',
}


def csv_to_ofx(csvfile, ofxfile):
    ofx = OFX(mapping)
    records = read_csv(csvfile)
    groups = ofx.gen_groups(records)
    trxns = ofx.gen_trxns(groups)
    cleaned_trxns = ofx.clean_trxns(trxns)
    data = utils.gen_data(cleaned_trxns)
    content = it.chain([ofx.header(), ofx.gen_body(data), ofx.footer()])
    for line in IterStringIO(content):
        ofxfile.write(line + b"\n")


def csv_to_qif(csvfile, qiffile):
    qif = QIF(mapping)
    records = read_csv(csvfile)
    groups = qif.gen_groups(records)
    trxns = qif.gen_trxns(groups)
    cleaned_trxns = qif.clean_trxns(trxns)
    data = utils.gen_data(cleaned_trxns)
    content = it.chain([qif.gen_body(data), qif.footer()])
    for line in IterStringIO(content):
        qiffile.write(line + b"\n")


def get_account_from_desc(desc, amount):
    if amount >= 0:
        return "Entrate"
    else:
        return "Uscite"


def iwbank_to_csv(fname, csvfile):
    writer = csv.writer(csvfile, delimiter=";")
    workbook = xlrd.open_workbook(fname)
    xl_sheet = workbook.sheet_by_index(0)
    (sx, sy) = (-1, -1)
    num_cols = xl_sheet.ncols
    # writer.writerow(["Date", "Amount", "Description", "Account"])
    for row_idx in range(0, xl_sheet.nrows):
        for col_idx in range(0, num_cols):
            cell_obj = xl_sheet.cell(row_idx, col_idx)
            if sx != -1 and col_idx == sx and row_idx > sy:
                date = xl_sheet.cell(row_idx, col_idx).value
                # FIXME: format date as month/day/year
                amount = xl_sheet.cell(row_idx, col_idx + 2).value
                desc = xl_sheet.cell(row_idx, col_idx + 4).value
                account = get_account_from_desc(desc, amount)
                # writer.writerow([date, amount, desc, account])
                writer.writerow([date, 0, "", "", desc, amount, "", ""])
            elif cell_obj.value == 'Data contabile':
                (sx, sy) = (col_idx, row_idx)


fname = sys.argv[1]

with open("temp.csv", "w", newline="") as csvfile:
    iwbank_to_csv(fname, csvfile)

# with open("out.ofx", "wb") as ofxfile:
#     csv_to_ofx("temp.csv", ofxfile)
#
# with open("out.qif", "wb") as ofxfile:
#     csv_to_qif("temp.csv", ofxfile)
